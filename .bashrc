readonly BASH_HOME="/home/allele/dotfiles/bash"

export LANG=en_US.utf8

source "$BASH_HOME/standard"
source "$BASH_HOME/path"
source "$BASH_HOME/completions"
source "$BASH_HOME/aliases"
source "$BASH_HOME/pretty"

# non-repo files
source "$BASH_HOME/priv/lpass"

title(){
   echo -en "\033]0;$1\a"
}

# OPAM configuration
. /home/allele/.opam/opam-init/init.sh > /dev/null 2> /dev/null || true

export TEXINFO='/home/allele/tools/texlive/texmf-dist/doc/info'
export INFOPATH="$TEXINFO"

export MANTEX='/home/allele/tools/texlive/texmf-dist/doc/man'
export MANPATH="$MANTEX"

# PATH="/home/allele/perl5/bin${PATH+:}${PATH}"; export PATH;
PERL5LIB="/home/allele/perl5/lib/perl5${PERL5LIB+:}${PERL5LIB}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/allele/perl5${PERL_LOCAL_LIB_ROOT+:}${PERL_LOCAL_LIB_ROOT}"
export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/allele/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/allele/perl5"; export PERL_MM_OPT;

CAML_LD_LIBRARY_PATH="/home/allele/.opam/system/lib/stublibs:/usr/lib/ocaml/stublibs"
export CAML_LD_LIBRARY_PATH;
MANPATH="/home/allele/.opam/system/man:/home/allele/tools/texlive/texmf-dist/doc/man:/usr/share/man"
export MANPATH;
PERL5LIB="/home/allele/.opam/system/lib/perl5:/home/allele/perl5/lib/perl5:/home/allele/perl5/lib/perl5"
export PERL5LIB;

OCAML_TOPLEVEL_PATH="/home/allele/.opam/system/lib/toplevel"; export OCAML_TOPLEVEL_PATH;

export NVM_DIR="/home/allele/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

SSH_ENV="$HOME/.ssh/environment"

function start_agent {
    echo "Initialising new SSH agent..."
    /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
    echo succeeded
    chmod 600 "${SSH_ENV}"
    . "${SSH_ENV}" > /dev/null
    /usr/bin/ssh-add;
}

export GPG_TTY="$(tty)"

# start psql server
if [ -z "$(pgrep postgres)" ]; then
   pg_ctl -D /home/allele/data/ -l /home/allele/logs/server.log start
fi

# nix
# . /home/allele/.nix-profile/etc/profile.d/nix.sh

# disable caps lock

if [ "$DISPLAY" != "" ]; then
  xmodmap -e "keycode 90 ="
  if [ -x /usr/bin/setxkbmap ]; then
    setxkbmap -option caps:none
    setxkbmap -option compose:rctrl
  fi
fi

if [ -e $HOME/.nix-profile/etc/profile.d/nix.sh ]; then
    . $HOME/.nix-profile/etc/profile.d/nix.sh
fi

JDK_HOME=$HOME/tools/jdk1.8.0_05/bin
JAVA_HOME=$HOME/tools/jdk1.8.0_05/bin

# Source SSH settings, if applicable

if [ -f "${SSH_ENV}" ]; then
    . "${SSH_ENV}" > /dev/null
    #ps ${SSH_AGENT_PID} doesn't work under cywgin
    ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
        start_agent;
    }
else
    start_agent;
fi
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# chruby: Dec 2022
if [ -d /home/allele/tools/chruby ]; then
  source /home/allele/tools/chruby/share/chruby/chruby.sh
  source /home/allele/tools/chruby/share/chruby/auto.sh
fi
