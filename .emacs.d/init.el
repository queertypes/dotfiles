;;; package -- summary
;;; Commentary:
;;;    Allele's init.el
;;; Code:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                      Custom Set Variables                                 ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   (vector "#eaeaea" "#d54e53" "#b9ca4a" "#e7c547" "#7aa6da" "#c397d8" "#70c0b1" "#000000"))
 '(compilation-message-face (quote default))
 '(custom-safe-themes
   (quote
    ("28caf31770f88ffaac6363acfda5627019cac57ea252ceb2d41d98df6d87e240" "a2cde79e4cc8dc9a03e7d9a42fabf8928720d420034b66aecc5b665bbf05d4e9" "947190b4f17f78c39b0ab1ea95b1e6097cc9202d55c73a702395fc817f899393" "f633d825e380caaaefca46483f7243ae9a663f6df66c5fad66d4cab91f731c86" "aaffceb9b0f539b6ad6becb8e96a04f2140c8faa1de8039a343a4f1e009174fb" "3860a842e0bf585df9e5785e06d600a86e8b605e5cc0b74320dfe667bcbe816c" "a9cc70b77b73121f900db49be622a8230529a02a40c22a384860cdc1d51d5494" "551fbb67f814e6cfb08ae0a9d52fbab0f9be91b1f6baeb58000c8a4e2e9c2f1c" "77b8019ceeaa58281944e503be6d2ed60d30685460389467e9975a1bc8b1f019" "5f93c1cff1bc77f68637dfbe207f46b08edf18c609fe2a4c7bb159b8d64f28ef" "ecb7bd2cf61c3918c03dcf63b4e25b9ef677d57a90e3f4d142444ea422f703f1" "233bb646e100bda00c0af26afe7ab563ef118b9d685f1ac3ca5387856674285d" "ff7625ad8aa2615eae96d6b4469fcc7d3d20b2e1ebc63b761a349bebbb9d23cb" "8ed752276957903a270c797c4ab52931199806ccd9f0c3bb77f6f4b9e71b9272" "c0155fc5592601de74e3071998d2291bc9801e113109371915b06c65d2631837" "0b6cb9b19138f9a859ad1b7f753958d8a36a464c6d10550119b2838cedf92171" "f78de13274781fbb6b01afd43327a4535438ebaeec91d93ebdbba1e3fba34d3c" "a800120841da457aa2f86b98fb9fd8df8ba682cebde033d7dbf8077c1b7d677a" "b9293d120377ede424a1af1e564ba69aafa85e0e9fd19cf89b4e15f8ee42a8bb" "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "38ba6a938d67a452aeb1dada9d7cdeca4d9f18114e9fc8ed2b972573138d4664" "f5eb916f6bd4e743206913e6f28051249de8ccfd070eae47b5bde31ee813d55f" "0fb6369323495c40b31820ec59167ac4c40773c3b952c264dd8651a3b704f6b5" "50d7344fa0fd20ac26c4294a42255a9c537723c9e03fa8eb8848593b876ffa2f" "d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "a12a28bf82f4634018f8d19f3d78fa34bd6c56dafe75db107852eb7b6a5a05b4" "50ba38d439ebd0ebba0c44003b21e1cbb99c811017f75325b2df53145bfbf1cf" "c7e8605c82b636fc489340e8276a3983745891e18e77440bbae305d1b5af9201" "b23e26a967ecc3841aed77d318005abad370fa1136cbb66dfd9899a4dc3e41e3" "7153b82e50b6f7452b4519097f880d968a6eaf6f6ef38cc45a144958e553fbc6" "ab04c00a7e48ad784b52f34aa6bfa1e80d0c3fcacc50e1189af3651013eb0d58" "a0feb1322de9e26a4d209d1cfa236deaf64662bb604fa513cca6a057ddf0ef64" "04dd0236a367865e591927a3810f178e8d33c372ad5bfef48b5ce90d4b476481" "7dd0db710296c4cec57c39068bfffa63861bf919fb6be1971012ca42346a417f" "c0dd5017b9f1928f1f337110c2da10a20f76da0a5b14bb1fec0f243c4eb224d4" "a301332a57e8de1b2996ee2d0b2439c18bd0cec9f8cc6ccaa73fac6e239462a8" "326d008ee4558bbfcd17d07686647f2f42c74a4161aea72089b6a51fc9974dbc" "f0d8af755039aa25cd0792ace9002ba885fd14ac8e8807388ab00ec84c9497d7" "b25040da50ef56b81165676fdf1aecab6eb2c928fac8a1861c5e7295d2a8d4dd" "b06aaf5cefc4043ba018ca497a9414141341cb5a2152db84a9a80020d35644d1" "05c3bc4eb1219953a4f182e10de1f7466d28987f48d647c01f1f0037ff35ab9a" "7ceb8967b229c1ba102378d3e2c5fef20ec96a41f615b454e0dc0bfa1d326ea6" "4e262566c3d57706c70e403d440146a5440de056dfaeb3062f004da1711d83fc" "31a01668c84d03862a970c471edbd377b2430868eccf5e8a9aec6831f1a0908d" "1297a022df4228b81bc0436230f211bad168a117282c20ddcba2db8c6a200743" "8db4b03b9ae654d4a57804286eb3e332725c84d7cdab38463cb6b97d5762ad26" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "64581032564feda2b5f2cf389018b4b9906d98293d84d84142d90d7986032d33" "9dae95cdbed1505d45322ef8b5aa90ccb6cb59e0ff26fef0b8f411dfc416c552" "628278136f88aa1a151bb2d6c8a86bf2b7631fbea5f0f76cba2a0079cd910f7d" "1b8d67b43ff1723960eb5e0cba512a2c7a2ad544ddb2533a90101fd1852b426e" "bb08c73af94ee74453c90422485b29e5643b73b05e8de029a6909af6a3fb3f58" "82d2cac368ccdec2fcc7573f24c3f79654b78bf133096f9b40c20d97ec1d8016" "06f0b439b62164c6f8f84fdda32b62fb50b6d00e8b01c2208e55543a6337433a" "4cf3221feff536e2b3385209e9b9dc4c2e0818a69a1cdb4b522756bcdf4e00a4" "4aee8551b53a43a883cb0b7f3255d6859d766b6c5e14bcb01bed572fcbef4328" "e16a771a13a202ee6e276d06098bc77f008b73bbac4d526f160faa2d76c1dd0e" "1e7e097ec8cb1f8c3a912d7e1e0331caeed49fef6cff220be63bd2a6ba4cc365" "fc5fcb6f1f1c1bc01305694c59a1a861b008c534cae8d0e48e4d5e81ad718bc6" default)))
 '(fci-rule-color "#424242")
 '(haskell-indentation-layout-offset 2)
 '(haskell-indentation-left-offset 2)
 '(haskell-process-auto-import-loaded-modules t)
 '(haskell-process-log t)
 '(haskell-process-suggest-remove-import-lines t)
 '(highlight-changes-colors ("#FD5FF0" "#AE81FF"))
 '(highlight-tail-colors
   (quote
    (("#49483E" . 0)
     ("#67930F" . 20)
     ("#349B8D" . 30)
     ("#21889B" . 50)
     ("#968B26" . 60)
     ("#A45E0A" . 70)
     ("#A41F99" . 85)
     ("#49483E" . 100))))
 '(lsp-eldoc-render-all nil)
 '(lsp-prefer-flymake nil)
 '(magit-diff-use-overlays nil)
 '(org-agenda-files
   (quote
    ("~/org/queertypes-agenda.org" "~/org/my-agenda.org")))
 '(package-selected-packages
   (quote
    (lsp-pyright rbs-mode quelpa-use-package quelpa tree-sitter typescript-mode org-pdfview esup yaml-mode js-mode lsp-java erlang js2-mode purescript-mode dante attrap nix-mode poe-filter-mode-pkg package-lint graphviz-dot-mode fsharp-mode elpy flycheck-golangci-lint docker-mode eglot company-lsp omnisharp flycheck-rust rustic racer bitlbee rspec-mode lsp-go lsp-ruby rebecca-theme lsp-haskell lsp-ui lsp-mode yard-mode robe enh-ruby-mode go-mode dot-mode protobuf-mode intero dracula-theme autodisass-java-bytecode hide-comnt groovy-mode gradle-mode poe-filter-mode window-number web-beautify use-package tuareg sml-modeline sml-mode smart-mode-line rust-mode rainbow-delimiters racket-mode python-mode psc-ide paredit org-journal org monokai-theme markdown-mode magit lua-mode json-mode js3-mode idris-mode grandshell-theme ensime dockerfile-mode cycbuf company-quickhelp cider)))
 '(pos-tip-background-color "#A6E22E")
 '(pos-tip-foreground-color "#272822")
 '(safe-local-variable-values
   (quote
    ((dante-methods stack)
     (intero-targets "vw:lib" "vw:exe:frmn-vw-fuzz" "vw:bench:frmn-vw-bench")
     (intero-targets "vw:lib" "vw:test:frmn-vw-fuzz")
     (haskell-indentation-where-post-offset . 2)
     (haskell-indentation-where-pre-offset . 2)
     (haskell-indentation-ifte-offset . 4)
     (haskell-indentation-left-offset . 4)
     (haskell-indentation-starter-offset . 2)
     (haskell-indentation-layout-offset . 4))))
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#d54e53")
     (40 . "#e78c45")
     (60 . "#e7c547")
     (80 . "#b9ca4a")
     (100 . "#70c0b1")
     (120 . "#7aa6da")
     (140 . "#c397d8")
     (160 . "#d54e53")
     (180 . "#e78c45")
     (200 . "#e7c547")
     (220 . "#b9ca4a")
     (240 . "#70c0b1")
     (260 . "#7aa6da")
     (280 . "#c397d8")
     (300 . "#d54e53")
     (320 . "#e78c45")
     (340 . "#e7c547")
     (360 . "#b9ca4a"))))
 '(vc-annotate-very-old-color nil)
 '(weechat-color-list
   (unspecified "#272822" "#49483E" "#A20C41" "#F92672" "#67930F" "#A6E22E" "#968B26" "#E6DB74" "#21889B" "#66D9EF" "#A41F99" "#FD5FF0" "#349B8D" "#A1EFE4" "#F8F8F2" "#F8F8F0")))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(flycheck-error ((t (:background "color-141" :foreground "color-161" :underline t :weight bold))))
 '(flycheck-warning ((t (:background "#4444ff" :foreground "#003311" :underline t :weight bold)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        Emacs Core Config                                  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; set GC threshold to 50MB
(setq gc-cons-threshold (* 50 1000 1000))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        Package Management                                 ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'package)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))

(package-initialize)

;; install package manager if it's not installed
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Enable defer and ensure by default for use-package
;; Keep auto-save/backup files separate from source code:
;;   https://github.com/scalameta/metals/issues/1027
(require 'use-package)
(setq use-package-always-defer t
      use-package-always-ensure t
      backup-directory-alist `((".*" . ,temporary-file-directory))
      auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                      Package Installations                                ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package flycheck
  :ensure t
  :defer t
  :init
  (global-flycheck-mode t))

(use-package yasnippet
  :defer t
  :ensure t)

(use-package smart-mode-line
  :ensure t)

(use-package cycbuf
  :ensure t)

(use-package window-number
  :ensure t)

(use-package rainbow-delimiters
  :defer t
  :ensure t)

(use-package projectile
  :defer t
  :ensure t)

(use-package company
  :defer t
  :ensure t)

(use-package lsp-mode
  :defer t
  :ensure t)

(use-package lsp-ui
  :defer t
  :ensure t)

(use-package company-lsp
  :defer t
  :ensure t)

(use-package eglot
  :defer t
  :ensure t)

(use-package magit
  :defer t
  :ensure t)

;; language syntax highlihting for free
(use-package tree-sitter
  :ensure t
  :config
  ;; activate tree-sitter on any buffer containing code for which it has a parser available
  (global-tree-sitter-mode)
  ;; you can easily see the difference tree-sitter-hl-mode makes for python, ts or tsx
  ;; by switching on and off
  (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))

(use-package tree-sitter-langs
  :ensure t
  :after tree-sitter)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                          User Interface                                   ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(when (fboundp 'windmove-default-keybindings)
    (windmove-default-keybindings))

;; encoding
(prefer-coding-system       'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)

;; time-display
(setq display-time-day-and-date nil
      display-time-24hr-format t)
(display-time)

;; ido
(require 'ido)
(ido-mode t)

;; status bar
(setq sml/theme 'respectful)
(sml/setup)

(global-set-key "\M-[1;5C" 'forward-word)   ;  Ctrl+right->forward word
(global-set-key "\M-[1;5D" 'backward-word)  ;  Ctrl+left-> backward word
(global-set-key "\M-[1;5B" 'forward-paragraph)
(global-set-key "\M-[1;5A" 'backward-paragraph)

(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-iswitchb)

(add-hook 'before-save-hook 'delete-trailing-whitespace)
(setq-default inhibit-startup-message t
              initial-scratch-message nil
              global-font-lock-mode t
              user-full-name "Allele Dev"
              user-mail-address "allele.dev@gmail.com"
              column-number-mode t
              line-number-mode t
              linum-format "%d "
              indent-tabs-mode nil
              tab-width 4
              js-indent-level 2
              auto-save-default nil)

;; backups
(defvar --backup-directory (concat user-emacs-directory "backups"))
(if (not (file-exists-p --backup-directory))
    (make-directory --backup-directory t))
(setq backup-directory-alist `(("." . ,--backup-directory)))
(setq make-backup-files t
      backup-by-copying t
      version-control t
      delete-old-versions t
      delete-by-moving-to-trash t
      kept-old-versions 6
      kept-new-versions 9
      auto-save-default nil
      auto-save-timeout 20
      auto-save-interval 200)

(menu-bar-mode -1)
(tool-bar-mode -1)
(defconst font-lock-maximum-decoration t)
(global-font-lock-mode t)
(setq font-lock-maximum-decoration t)
(display-time-mode 1)

;; Better etags support
;; - don't prompt when finding a tag
(defun find-tag-no-prompt ()
  "Jump to the tag at point without prompting."
  (interactive)
  (xref-find-definitions (find-tag-default)))
(global-set-key (kbd "M-.") 'find-tag-no-prompt)

(global-set-key [(control x) (k)] 'kill-this-buffer)
(global-set-key (kbd "<f6>") 'magit-status)

;; window-number
(require 'window-number)
(window-number-mode)
(window-number-meta-mode)

;; unicode support
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8-unix)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

;; cycbuf
(add-hook 'after-init-hook 'cycbuf-init)
(global-set-key [(f9)] 'cycbuf-switch-to-next-buffer)
(global-set-key [(f8)] 'cycbuf-switch-to-previous-buffer)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        [Styling & Themes]                                 ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package grandshell-theme
  :ensure t
  :defer t)

(use-package monokai-theme
  :ensure t
  :defer t)

(use-package rebecca-theme
  :ensure t
  :defer t)

(use-package dracula-theme
  :ensure t
  :defer t)

;; default theme
(load-theme 'dracula t)

;; guarantee some consistency among font faces below
(require 'font-lock)

(defun --copy-face (new-face face)
  "Define NEW-FACE from existing FACE."
  (copy-face face new-face)
  (eval `(defvar ,new-face nil))
  (set new-face new-face))

(--copy-face 'font-lock-label-face  ; labels, case, public, private, proteced, namespace-tags
         'font-lock-keyword-face)
(--copy-face 'font-lock-doc-markup-face ; comment markups such as Javadoc-tags
         'font-lock-doc-face)
(--copy-face 'font-lock-doc-string-face ; comment markups
         'font-lock-comment-face)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                            Load Path                                      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-to-list 'load-path "~/.emacs.d/elisp/")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                              Backup                                       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq
   backup-by-copying t      ; don't clobber symlinks
   backup-directory-alist
    '(("." . "~/.saves"))    ; don't litter my fs tree
   delete-old-versions t
   kept-new-versions 6
   kept-old-versions 2
   version-control t
   auto-save-default t
   auto-save-timeout 20
   auto-save-interval 200
   )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                   Programming Language Support                   ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'flycheck)

(define-key global-map "\C-n" 'flycheck-next-error)
(define-key global-map "\C-p" 'flycheck-previous-error)

;; flycheck
;;(global-set-key (kbd "C-n") 'next-error)
;;(global-set-key (kbd "C-p") 'previous-error)


(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        [Table of Contents]                                ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; [Markdown]
;; [Haskell]
;; [Purescript]
;; [Javascript]
;; [Idris]
;; [Clojure]
;; [Racket]
;; [Elisp]
;; [Ruby]
;; [Erlang]
;; [Org Mode]
;; [OCaml]
;; [Path of Exile]
;; [Java]
;; [CSharp]
;; [Rust]
;; [JSON]
;; [Docker]
;; [Scala]
;; [Golang]
;; [C/C++]
;; [Coq]
;; [Python]
;; [FSharp]
;; [Graphviz Dot]
;; [Gradle]
;; [Groovy]
;; [Standard ML]
;; [Nix]
;; [Lua]
;; [Typescript]

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                           [Markdown]                                      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package markdown-mode
             :ensure t
             :commands (markdown-mode gfm-mode)
             :mode (("README\\.md\\'" . gfm-mode)
                    ("\\.md\\'" . markdown-mode)
                    ("\\.markdown\\'" . markdown-mode)
                    ("changelog.md" . markdown-mode))
             :init (setq markdown-command "multimarkdown"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                            [Haskell]                                      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package attrap
  :defer t
  :ensure t)

(use-package dante
  :ensure t
  :after haskell-mode
  :commands 'dante-mode
  :hook ((haskell-mode . flycheck-mode)
         (haskell-mode . rainbow-delimiters-mode)
         (haskell-mode . subword-mode)
         (haskell-mode . dante-mode))
  :init
  (add-hook 'dante-mode-hook
            '(lambda () (flycheck-add-next-checker 'haskell-dante
                                                   '(warning . haskell-hlint))))
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                          [Purescript]                                     ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package psc-ide
  :defer t
  :ensure t)

(use-package purescript-mode
  :ensure t
  :hook ((purescript-mode . rainbow-delimiters-mode)
         (purescript-mode . company-mode)
         (purescript-mode . psc-ide-mode)
         (purescript-mode . flycheck-mode))
  :init
  (turn-on-purescript-indentation)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                           [Javascript]                                    ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-hook 'js-mode-hook 'flycheck-mode)
(add-hook 'js-mode-hook 'rainbow-delimiters-mode)

(setq-default flycheck-disabled-checkers
              (append flycheck-disabled-checkers
                      '(javascript-jshint)))

;; TODO: add support for flow and other checkers/linters

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                             [Idris]                                       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package idris-mode
  :ensure t
  :defer t
  :init
  (add-hook 'idris-mode-hook 'flycheck-mode)
  (add-hook 'idris-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'idris-mode-hook 'subword-mode))

(require 'idris-mode)

(flycheck-define-checker idris-checker
  "An Idris syntax and type checker."
  :command ("idris" "--check" "--nocolor" "--warnpartial" source)
  :error-patterns
  ((warning line-start (file-name) ":" line ":" column ":Warning - "
            (message (and (* nonl) (* "\n" (not (any "/" "~")) (* nonl)))))
   (error line-start (file-name) ":" line ":" column ":"
          (message (and (* nonl) (* "\n" (not (any "/" "~")) (* nonl))))))
  :modes idris-mode)

(add-to-list 'flycheck-checkers 'idris-checker)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                            [Clojure]                                      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package clojure-mode
  :ensure t
  :defer t
  :init
  (add-hook 'clojure-mode-hook 'flycheck-mode)
  (add-hook 'clojure-mode-hook 'rainbow-delimiters-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                              [Racket]                                     ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package racket-mode
  :ensure t
  :defer t
  :init
  (add-hook 'racket-mode-hook 'flycheck-mode)
  (add-hook 'racket-mode-hook 'rainbow-delimiters-mode))

(add-to-list 'auto-mode-alist '("\\.rkt\\'" . racket-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                             [Elisp]                                       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-hook 'emacs-lisp-mode-hook 'flycheck-mode)
(add-hook 'emacs-lisp-mode-hook 'rainbow-delimiters-mode)
(add-hook 'emacs-lisp-mode--hook 'paredit-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                              [Ruby]                                       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package lsp-ruby
  :defer t
  :ensure t)

(use-package yard-mode
  :defer t
  :ensure t)

(use-package robe
  :defer t
  :ensure t)

(use-package rspec-mode
  :defer t
  :ensure t)

(use-package enh-ruby-mode
  :ensure t
  :defer t
  :init
  (add-hook 'enh-ruby-mode-hook 'flycheck-mode)
  (add-hook 'enh-ruby-mode-hook 'robe-mode)
  (add-hook 'enh-ruby-mode-hook 'yard-mode)

  (require 'lsp-ruby)
  (add-hook 'enh-ruby-mode-hook #'lsp-ruby-enable)
  (add-hook 'enh-ruby-mode-hook 'rainbow-delimiters-mode)
  (remove-hook 'enh-ruby-mode-hook 'erm-define-faces)
  (add-to-list 'auto-mode-alist '("\\.rb$" . enh-ruby-mode))
  (add-to-list 'auto-mode-alist '("Gemfile" . enh-ruby-mode))

  (setq lsp-hover-text-function 'lsp--text-document-signature-help)
  )

;; TODO: add support for sorbet and other checkers/linters

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                             [Erlang]                                      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(flycheck-define-checker erlang-dialyzer
  "A simple flycheck scheme for integrating with dialyzer."
  :command ("dialyzer" "-I" "." "--src" source)
  :error-patterns
  ((warning line-start (file-name) ":" line ": " (message) line-end))
  :modes erlang-mode)

(use-package erlang
  :ensure t
  :defer t
  :init
  (add-hook 'erlang-mode-hook 'flycheck-mode)
  (add-hook 'erlang-mode-hook 'rainbow-delimiters-mode)
  (add-to-list 'flycheck-checkers 'erlang-dialyzer)
  (setq erlang-electric-commands '()))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                            [Org Mode]                                     ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package org
  :ensure t
  :defer t
  :init
  (add-hook 'org-mode-hook 'rainbow-delimiters-mode))

(define-key global-map "\C-cc" 'org-capture)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                             [OCaml]                                       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package tuareg
  :ensure t
  :defer t
  :init
  (add-hook 'tuareg-mode-hook 'flycheck-mode)
  (add-hook 'tuareg-mode-hook 'rainbow-delimiters-mode))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                         [Path of Exile]                                   ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; TODO: upload poe-filter-mode to Melpa
(add-to-list 'auto-mode-alist '("\\.filter\\'" . poe-filter-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                              [Java]                                       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package autodisass-java-bytecode
  :defer t
  :ensure t)

(use-package lsp-java
  :ensure t
  :hook (
    (java-mode . rainbow-delimiters-mode)
    (java-mode . company-mode)
    (java-mode . flycheck-mode)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                            [CSharp]                                       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package omnisharp
  :ensure t
  :defer t)

(use-package csharp-mode
  :ensure t
  :hook ((csharp-mode . rainbow-delimiters-mode)
         (csharp-mode . flycheck-mode)
         (csharp-mode . omnisharp-mode)
         (csharp-mode . company-mode))
  :init
  (setq indent-tabs-mode nil)
  (setq c-syntactic-indentation t)
  (setq c-basic-offset 4)
  (setq truncate-lines t)
  (setq tab-width 4)

  (electric-pair-local-mode 1)

  (require 'company)
  (add-to-list 'company-backends #'company-omnisharp)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                             [Rust]                                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package racer
  :defer t
  :ensure t)

(use-package flycheck-rust
  :defer t
  :ensure t)

(use-package rust-mode
  :defer t
  :ensure t)

(use-package rustic
  :ensure t
  :hook ((rust-mode . rainbow-delimiters-mode)
         (rust-mode . company-mode)
         (rust-mode . cargo-minor-mode)
         (rust-mode . racer-mode)
         (rust-mode . flycheck-mode))
  :bind-keymap ("C-c <tab>" . rust-format-buffer)
  )

(require 'flycheck)
(add-hook 'flycheck-mode #'flycheck-rust-setup)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                              [JSON]                                       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package json-mode
  :ensure t
  :hook ((json-mode . rainbow-delimiters-mode)
         (json-mode . flycheck-mode)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                             [Docker]                                      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package dockerfile-mode
  :ensure t
  :hook ((dockerfile-mode . rainbow-delimiters-mode)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                              [YAML]                                       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package yaml-mode
  :ensure t
  :hook ((yaml-mode . rainbow-delimiters-mode)
         (yaml-mode . flycheck-mode)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                             [Scala]                                       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Enable scala-mode and sbt-mode
(use-package scala-mode
  :mode "\\.s\\(cala\\|bt\\)$"
  :hook ((scala-mode . rainbow-delimiters-mode)
         (scala-mode . flycheck-mode)))

(use-package sbt-mode
  :commands sbt-start sbt-command
  :hook ((sbt-mode . rainbow-delimiters-mode)
         (sbt-mode . flycheck-mode))
  :config
  ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
  ;; allows using SPACE when in the minibuffer
  (substitute-key-definition
   'minibuffer-complete-word
   'self-insert-command
   minibuffer-local-completion-map)
   ;; sbt-supershell kills sbt-mode:  https://github.com/hvesalai/emacs-sbt-mode/issues/152
   (setq sbt:program-options '("-Dsbt.supershell=false"))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                             [Golang]                                      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package flycheck-golangci-lint
  :ensure t)

(use-package go-mode
  :ensure t
  :hook ((go-mode . rainbow-delimiters-mode)
         (go-mode . flycheck-mode)
         (go-mode . projectile)
         (go-mode . flycheck-golangci-lint))
  :init
  (require 'flycheck-golangci-lint)
  (setq flycheck-golangci-lint-deadline "5s")
  (setq flycheck-golangci-lint-tests t)
  (setq flycheck-golangci-lint-fast t)

  (eval-after-load 'flycheck
    '(add-hook 'flycheck-mode-hook #'flycheck-golangci-lint-setup))

  (add-hook 'before-save-hook #'gofmt-before-save))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                             [C/C++]                                       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'eglot)
(add-to-list 'eglot-server-programs '((c++-mode c-mode) "clangd"))
(add-hook 'c-mode-hook 'eglot-ensure)
(add-hook 'c++-mode-hook 'eglot-ensure)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                             [Python]                                      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package python-mode
  :defer t
  :ensure t
  :hook ((python-mode . rainbow-delimiters-mode)
         (python-mode . flycheck-mode))
  )

(use-package lsp-pyright
  :ensure t
  :hook (python-mode . (lambda ()
                          (require 'lsp-pyright)
                          (lsp))))  ; or lsp-deferred

;; (use-package elpy
;;   :ensure t
;;   :defer t
;;   :init
;;   (advice-add 'python-mode :before 'elpy-enable)
;;   )
;; TODO: add support for mypy and other checkers/linters

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                             [FSharp]                                      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package fsharp-mode
  :defer t
  :ensure t
  :hook ((fsharp-mode . rainbow-delimiters-mode)
         (fsharp-mode . flycheck-mode)
         ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                          [Graphviz Dot]                                   ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package graphviz-dot-mode
  :defer t
  :ensure t
  :hook ((graphviz-dot-mode . rainbow-delimiters-mode)
         (graphviz-dot-mode . flycheck-mode)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                             [Gradle]                                      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package gradle-mode
  :defer t
  :ensure t
  :hook ((gradle-mode . rainbow-delimiters-mode)
         (gradle-mode . flycheck-mode)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                             [Groovy]                                      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package groovy-mode
  :defer t
  :ensure t
  :hook ((groovy-mode . rainbow-delimiters-mode)
         (groovy-mode . flycheck-mode)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                          [Standard ML]                                    ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package sml-mode
  :defer t
  :ensure t
  :hook ((sml-mode . rainbow-delimiters-mode)
         (sml-mode . flycheck-mode)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                              [Nix]                                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package nix-mode
  :defer t
  :ensure t
  :mode "\\.nix\\'"
  :hook ((nix-mode . rainbow-delimiters-mode)
         (nix-mode . flycheck-mode)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                              [Lua]                                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package lua-mode
  :defer t
  :ensure t
  :hook ((lua-mode . rainbow-delimiters-mode)
         (lua-mode . flycheck-mode)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                          [Typescript]                                     ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package typescript-mode
  :after tree-sitter
  :config
  ;; we choose this instead of tsx-mode so that eglot can automatically figure out language for server
  ;; see https://github.com/joaotavora/eglot/issues/624 and https://github.com/joaotavora/eglot#handling-quirky-servers
  (define-derived-mode typescriptreact-mode typescript-mode
    "TypeScript TSX")

  ;; use our derived mode for tsx files
  (add-to-list 'auto-mode-alist '("\\.tsx?\\'" . typescriptreact-mode))
  ;; by default, typescript-mode is mapped to the treesitter typescript parser
  ;; use our derived mode to map both .tsx AND .ts -> typescriptreact-mode -> treesitter tsx
  (add-to-list 'tree-sitter-major-mode-language-alist '(typescriptreact-mode . tsx))
  :hook ((typescript-mode . rainbow-delimiters-mode))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        Start-Up Profiling                                 ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Use a hook so the message doesn't get clobbered by other messages.
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs ready in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))

;;; init.el ends here
